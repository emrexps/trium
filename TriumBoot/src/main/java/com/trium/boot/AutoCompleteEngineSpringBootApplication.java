package com.trium.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoCompleteEngineSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutoCompleteEngineSpringBootApplication.class, args);
	}

}
